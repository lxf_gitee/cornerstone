/**
 * Copyright (C), 2015-2019, XXX有限公司
 * FileName: SimpleRepository
 * Author:   liyuan
 * Date:     2019-03-21 15:36
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.cs.bs.es.dao;


import com.cs.bs.es.entity.Simple;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author liyuan
 * @create 2019-03-21
 * @since 1.0.0
 */
@Repository
public interface SimpleRepository extends JpaRepository<Simple, Integer> {

}